#pragma once
#ifndef WALKER_PARAMS_DEFINED
#define WALKER_PARAMS_DEFINED
struct WalkerParams {
  float a;
  float al;
  float p;
  float pl;
  float omega;
  float omegal;
  float nu;
  float lambda;
  float lambdal;
  float m;
  float L;
  float fwdspeed;
};
#endif
